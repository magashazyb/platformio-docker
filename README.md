# README
Since platformio hijacks my normal atom installation(i.e. installs a lot of packages, changes the default ui and I dont want it to mess with my everyday atom config) I have created a docker image to run the platformio version inside.
This project is not ready yet, there are errors.

# Usage
##### 1. Clone the repo
`git clone https://magashazyb@bitbucket.org/magashazyb/platformio-docker.git`
##### 2. Build it
Run the docker compose up command. It will build the image from the attached Dockerfile. Furthermore it contains the necessary mappings for the container.
`docker-compose up`
It may take a while..
###### 2.1. First run
After the container has built an atom instance will pop up and in the backround it installs the platformio platform.
Looks like nothing happens but don't close the windows. After a while a green window will pop up in the upper right corner asking for a restart. Press it.
The conteiner now closes and the installation is done. Proceed to section 3.

##### 3. Start
Run `docker-compose up` command. If you would like to run it in the backround add the `-d` option to the command.
